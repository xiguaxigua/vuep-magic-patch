const SPLIT_SIGN = '@!@'
const DEFAULT_REPLICE_LIST = [
  { from: '<template>', to: SPLIT_SIGN + '<template>' },
  { from: '</template>', to: '</template>' + SPLIT_SIGN },
  { from: '<template>', to: '<div id="app">' },
  { from: '</template>', to: '</div>' },
  { from: '<style>', to: '' },
  { from: '</style>', to: '' },
  { from: '<script>', to: '' },
  { from: 'export default {', to: 'new Vue({\n    el: \'#app\',\n' },
  { from: '\n<\/script>', to: ')' }
]

export default {
  props: {
    value: String
  },
  computed: {
    content ({ value, getContent }) {
      return getContent(value)
    }
  },
  methods: {
    getContent (value) {
      let post = value
      const resource = post.match(/\[_(.*)_]/g)
      let reso = window.VUEP_RESOURCE && window.VUEP_RESOURCE.join(',') || ''
      if (resource) {
        const s = resource[0].replace(/(\[_|_]|\s)/g, '')
        reso = [reso, s].join(',')
      }
      const replaceList = window.VUEP_REPLICE_LIST || DEFAULT_REPLICE_LIST
      const sign = window.SIGN || SPLIT_SIGN
      replaceList.forEach(({ from, to }) => {
        post = post.replace(from, to)
      })
      post = post.split(sign).map(item => item.replace(/^\n+/gm, ''))
      post[6] = post[1]
      let codepenExtra = []
      if (window.VUEP_FORM_RESOURCE) {
        post[1] = `${window.VUEP_FORM_RESOURCE.join('\n')}\n${post[1]}`
        codepenExtra = window.VUEP_FORM_RESOURCE.map(item => {
          if (!item.indexOf('<script')) {
            return item.match(/<script src="(.*)"><(\\)?\/script>/)[1]
          }
          if (!item.indexOf('<link')) {
            return item.match(/<link rel="stylesheet" href="(.*)">/)[1]
          }
        })
      }
      post[1] = post[1].replace(/<!-- \[_(.*)_] -->/, '')
      post[2] = this.getFormat(post[2])
      post[3] = reso
      post[4] = ''
      post[5] = ''
      let resoArr = reso.split(',')
      if (codepenExtra.length) resoArr = resoArr.concat(codepenExtra)
      resoArr.forEach(item => {
        if (/\.js$/.test(item)) post[4] += `${item};`
        if (/\.css$/.test(item)) post[5] += `${item};`
      })
      return post
    },
    getFormat (str) {
      let indent = 0
      Array.prototype.some.call(str, s => {
        if (s !== ' ') return true
        indent++
      })
      const reg = new RegExp(`^(\\s{0,${indent}}|[\\n])`, 'gm')
      return str.replace(reg, '')
    }
  },
  render (h) {
    /* eslint-disable camelcase */
    const codepenData = JSON.stringify({
      css: this.content[0],
      html: this.content[6],
      js: this.content[2],
      js_external: this.content[4],
      css_external: this.content[5],
      layout: window.VUEP_CODEPEN_LAYOUT || 'left',
      js_pre_processor: window.VUEP_CODEPEN_JS_PROCESSOR || 'babel',
      editors: window.VUEP_CODEPEN_EDITORS || '101'
    })
    return h('div', {
      staticClass: 'vuep-out-link'
    }, [h('form', {
      staticClass: 'vuep-jsfiddle',
      domProps: {
        target: '_blank',
        action: 'https://jsfiddle.net/api/post/library/pure/',
        method: 'post'
      }
    }, [
      h('input', { domProps: { type: 'hidden', name: 'css', value: this.content[0] }}),
      h('input', { domProps: { type: 'hidden', name: 'html', value: this.content[1] }}),
      h('input', { domProps: { type: 'hidden', name: 'panel_js', value: 3 }}),
      h('input', { domProps: { type: 'hidden', name: 'wrap', value: 1 }}),
      h('input', { domProps: { type: 'hidden', name: 'js', value: this.content[2] }}),
      h('input', { domProps: { type: 'hidden', name: 'resources', value: this.content[3] }}),
      h('button', 'JSFiddle')
    ]), h('form', {
      staticClass: 'vuep-codepen',
      domProps: {
        target: '_blank',
        action: 'https://codepen.io/pen/define',
        method: 'post'
      }
    }, [
      h('input', { domProps: { type: 'hidden', name: 'data', value: codepenData }}),
      h('button', 'Codepen')
    ])])
  }
}
